-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Creato il: Gen 11, 2021 alle 22:02
-- Versione del server: 10.5.8-MariaDB-1:10.5.8+maria~focal
-- Versione PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `my_db`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `paper`
--

CREATE TABLE `paper` (
  `id` int(11) NOT NULL,
  `doi` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `pub_date` date NOT NULL,
  `ncit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `paper`
--

INSERT INTO `paper` (`id`, `doi`, `title`, `sub_title`, `pub_date`, `ncit`) VALUES
(45, '10.8888', 'Otto', 'vonBitsmark', '2020-08-08', 8),
(46, '10.9696', 'Riconoscimento efficiente di esoni', '', '2020-07-24', 1),
(47, '10.3333', 'Differential drive robots', '', '2019-06-04', 20),
(48, '10.1196', 'Integrazione di classificatori di ADL in App Android', '', '2020-07-23', 431),
(49, '10.4632', 'ASGAL', '', '2019-04-09', 111);

-- --------------------------------------------------------

--
-- Struttura della tabella `paper_to_author`
--

CREATE TABLE `paper_to_author` (
  `paper` int(11) NOT NULL,
  `author` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `paper_to_author`
--

INSERT INTO `paper_to_author` (`paper`, `author`) VALUES
(45, 31),
(45, 35),
(46, 32),
(47, 35),
(48, 30),
(49, 40);

-- --------------------------------------------------------

--
-- Struttura della tabella `paper_to_paper`
--

CREATE TABLE `paper_to_paper` (
  `paper` int(11) NOT NULL,
  `quoted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `paper_to_paper`
--

INSERT INTO `paper_to_paper` (`paper`, `quoted`) VALUES
(45, 47),
(46, 49);

-- --------------------------------------------------------

--
-- Struttura della tabella `paper_to_reviewer`
--

CREATE TABLE `paper_to_reviewer` (
  `paper` int(11) NOT NULL,
  `reviewer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `paper_to_reviewer`
--

INSERT INTO `paper_to_reviewer` (`paper`, `reviewer`) VALUES
(45, 33),
(45, 34),
(46, 34),
(48, 34),
(49, 33);

-- --------------------------------------------------------

--
-- Struttura della tabella `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `birth` date DEFAULT NULL,
  `hIndex` int(11) DEFAULT NULL,
  `i10Index` int(11) DEFAULT NULL,
  `ncit` int(11) DEFAULT NULL,
  `nrev` int(11) DEFAULT NULL,
  `university` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `person`
--

INSERT INTO `person` (`id`, `type`, `name`, `surname`, `birth`, `hIndex`, `i10Index`, `ncit`, `nrev`, `university`) VALUES
(30, 'author', 'Gabriele', 'De Rosa', '1996-11-12', 1212, 211, 1231, NULL, 33),
(31, 'author', 'Federica', 'Di Lauro', '1998-09-29', 142, 12, 80, NULL, 33),
(32, 'author', 'Davide', 'Cozzi', '1996-11-01', 96, 96, 96, NULL, 33),
(33, 'reviewer', 'Mario', 'Bianco', '2021-01-01', NULL, NULL, NULL, 2, 31),
(34, 'reviewer', 'Stefano', 'Giacomini', '1998-06-16', NULL, NULL, NULL, 67, 32),
(35, 'author', 'Simone', 'Fontana', '1987-09-09', 290, 49, 376, NULL, 33),
(40, 'author', 'Luca', 'Denti', '1986-01-06', 454, 456, 31, NULL, 33);

-- --------------------------------------------------------

--
-- Struttura della tabella `university`
--

CREATE TABLE `university` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `country` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `university`
--

INSERT INTO `university` (`id`, `name`, `country`, `city`) VALUES
(31, 'Università degli Studi di Milano', 'Italia', 'Milano'),
(32, 'Politecnico di Milano', 'Italia', 'Milano'),
(33, 'Università degli Studi di Milano - Bicocca', 'Italia', 'Milano');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `paper`
--
ALTER TABLE `paper`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `doi` (`doi`);

--
-- Indici per le tabelle `paper_to_author`
--
ALTER TABLE `paper_to_author`
  ADD PRIMARY KEY (`paper`,`author`),
  ADD KEY `author` (`author`),
  ADD KEY `paper` (`paper`);

--
-- Indici per le tabelle `paper_to_paper`
--
ALTER TABLE `paper_to_paper`
  ADD PRIMARY KEY (`paper`,`quoted`),
  ADD KEY `paper` (`paper`),
  ADD KEY `quoted` (`quoted`);

--
-- Indici per le tabelle `paper_to_reviewer`
--
ALTER TABLE `paper_to_reviewer`
  ADD PRIMARY KEY (`paper`,`reviewer`),
  ADD KEY `paper` (`paper`),
  ADD KEY `reviewer` (`reviewer`);

--
-- Indici per le tabelle `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `university` (`university`);

--
-- Indici per le tabelle `university`
--
ALTER TABLE `university`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `paper`
--
ALTER TABLE `paper`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT per la tabella `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT per la tabella `university`
--
ALTER TABLE `university`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `paper_to_author`
--
ALTER TABLE `paper_to_author`
  ADD CONSTRAINT `paper_to_author_ibfk_3` FOREIGN KEY (`author`) REFERENCES `person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `paper_to_author_ibfk_4` FOREIGN KEY (`paper`) REFERENCES `paper` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `paper_to_paper`
--
ALTER TABLE `paper_to_paper`
  ADD CONSTRAINT `paper_to_paper_ibfk_1` FOREIGN KEY (`paper`) REFERENCES `paper` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `paper_to_paper_ibfk_2` FOREIGN KEY (`quoted`) REFERENCES `paper` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `paper_to_reviewer`
--
ALTER TABLE `paper_to_reviewer`
  ADD CONSTRAINT `paper_to_reviewer_ibfk_2` FOREIGN KEY (`reviewer`) REFERENCES `person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `paper_to_reviewer_ibfk_3` FOREIGN KEY (`paper`) REFERENCES `paper` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limiti per la tabella `person`
--
ALTER TABLE `person`
  ADD CONSTRAINT `person_ibfk_1` FOREIGN KEY (`university`) REFERENCES `university` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
