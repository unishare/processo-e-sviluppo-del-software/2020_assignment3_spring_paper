package com.unishare.paperapp.controller;

import java.util.Optional;
import java.util.List;
import java.util.ArrayList;

import org.springframework.ui.Model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.unishare.paperapp.model.Paper;
import com.unishare.paperapp.model.Author;
import com.unishare.paperapp.model.Reviewer;
import com.unishare.paperapp.service.PaperService;
import com.unishare.paperapp.service.AuthorService;
import com.unishare.paperapp.service.ReviewerService;

import com.unishare.paperapp.exception.*;

@Controller
public class PaperController { 
	
	@Autowired
	private PaperService paperService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private ReviewerService reviewerService;
	
	@GetMapping("/papers")
	public ModelAndView papers() {
		ModelAndView modelAndView = new ModelAndView();
		
		Iterable<Paper> papers = paperService.getPapers();
		modelAndView.addObject("papers", papers);
		modelAndView.setViewName("papers");
		
		return modelAndView;
	}
	
	@GetMapping("/papers/{id}")
	public ModelAndView paper(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView();
		
		Optional<Paper> paper = paperService.getPaper(id);
		paper.orElseThrow(PaperNotFoundException::new);
		modelAndView.addObject("paper", paper.get());
		
		Iterable<Paper> citations = paperService.getPaperCitations(paper.get());
		modelAndView.addObject("citations", citations);

		Iterable<Paper> quotations = paperService.getPaperQuotations(paper.get());
		modelAndView.addObject("quotations", quotations);

		List<Author> authors = paper.get().getAuthors();
		modelAndView.addObject("authors", authors);
		
		List<Reviewer> reviewers = paper.get().getReviewers();
		modelAndView.addObject("reviewers", reviewers);
		
		modelAndView.setViewName("paper");
		return modelAndView;
	}
	
	@GetMapping("/papers/delete/{id}")
	public String deletePaper(@PathVariable("id") Long id) {
		try{
			paperService.deletePaper(id);
		} catch(Exception e) {
			throw new PaperAlreadyDeletedException();
		}
		return "redirect:/papers";
	}
	
	
	@GetMapping("/add_paper")
	public ModelAndView addPaper() {
		ModelAndView modelAndView = new ModelAndView();
		
		Paper paper = new Paper();
		modelAndView.addObject("paper", paper);
		
		modelAndView.setViewName("paper_add");
		return modelAndView;
	}
	
	@PostMapping(value = "/save_paper")
	public String savePaper(@ModelAttribute Paper paper) {
		paperService.addPaper(paper);
		return "redirect:/papers";
	}
	
	@GetMapping(value = "/papers/edit/{id}")
	public ModelAndView editPaper(@PathVariable("id") Long id){
		ModelAndView modelAndView = new ModelAndView();

		Optional<Paper> paper = paperService.getPaper(id);
		paper.orElseThrow(PaperNotFoundException::new);
		modelAndView.addObject("paper", paper.get());
		
		modelAndView.setViewName("paper_edit");
		return modelAndView;
	}
	
	@PostMapping(value = "/update_paper")
	public String updatePaper(@ModelAttribute Paper paper) {
		paperService.updatePaper(paper);
		
		return "redirect:/papers/" + paper.getId();
	}
	
	@GetMapping(value = "/papers/add_author/{id}")
	public ModelAndView addAuthor(@PathVariable("id") Long id){
		ModelAndView modelAndView = new ModelAndView();
		
		Optional<Paper> paper = paperService.getPaper(id);
		paper.orElseThrow(PaperNotFoundException::new);
		modelAndView.addObject("paper", paper.get());
		
		/**
		* Create a list of authors without the authors which are already in a 
		* relationship with the paper
		*/
		
		Iterable<Author> alreadyAuthors = paper.get().getAuthors();
		Iterable<Author> allAuthors = authorService.getAuthors();
		
		List<Author> authors = new ArrayList<Author>();		
		for (Author author : allAuthors) {
			boolean found = false;
			for (Author alreadyAuthor : alreadyAuthors) {
				if (author.equals(alreadyAuthor)) {
					found = true;
				}
			}
			if (!found) authors.add(author);
		}		
		modelAndView.addObject("authors", authors);
		
		modelAndView.setViewName("add_paper_author");
		return modelAndView;
	}
	
	@PostMapping(value = "/update_paper_author/{id}")
	public String updatePaperAuthor(@PathVariable("id") Long id, @RequestParam Long idAuthor) {
		Optional<Paper> paper = paperService.getPaper(id);
		paper.orElseThrow(PaperNotFoundException::new);
		
		Optional<Author> author = authorService.getAuthor(idAuthor);
		author.orElseThrow(AuthorNotFoundException::new);

		try{
			paperService.addAuthor(paper.get(), author.get());
		} catch(Exception e) {
			throw new AuthorAlreadyPresentException();
		}

		return "redirect:/papers/" + id;
	}
	
	@GetMapping(value = "/papers/add_reviewer/{id}")
	public ModelAndView addReviewer(@PathVariable("id") Long id){
		ModelAndView modelAndView = new ModelAndView();

		Optional<Paper> paper = paperService.getPaper(id);	
		paper.orElseThrow(PaperNotFoundException::new);
		modelAndView.addObject("paper", paper.get());
		
		/**
		* Create a list of reviewers without the reviewers which are already in a 
		* relationship with the paper
		*/
		
		Iterable<Reviewer> alreadyReviewers = paper.get().getReviewers();
		Iterable<Reviewer> allReviewers = reviewerService.getReviewers();
		
		List<Reviewer> reviewers = new ArrayList<Reviewer>();		
		for (Reviewer reviewer : allReviewers) {
			boolean found = false;
			for (Reviewer alreadyReviewer : alreadyReviewers) {
				if (reviewer.equals(alreadyReviewer)) {
					found = true;
				}
			}
			if (!found) reviewers.add(reviewer);
		}		
		modelAndView.addObject("reviewers", reviewers);
		
		modelAndView.setViewName("add_paper_reviewer");
		return modelAndView;
		
	}
	
	@PostMapping(value = "/update_paper_reviewer/{id}")
	public String updatePaperReviewer(@PathVariable("id") Long id, @RequestParam Long idReviewer) {
		Optional<Paper> paper = paperService.getPaper(id);
		paper.orElseThrow(PaperNotFoundException::new);
		
		Optional<Reviewer> reviewer = reviewerService.getReviewer(idReviewer);
		reviewer.orElseThrow(ReviewerNotFoundException::new);
		try{
			paperService.addReviewer(paper.get(), reviewer.get());
		} catch(Exception e) {
			throw new ReviewerAlreadyPresentException();
		}
		
		return "redirect:/papers/" + id;
	}
	
	@PostMapping(value = "/delete_paper_author/{id}")
	public String deletePaperAuthor(@PathVariable("id") Long id, @RequestParam Long idAuthor) {
		Optional<Paper> paper = paperService.getPaper(id);
		paper.orElseThrow(PaperNotFoundException::new);
		
		Optional<Author> author = authorService.getAuthor(idAuthor);
		author.orElseThrow(AuthorNotFoundException::new);
		
		paperService.deleteAuthor(paper.get(), author.get());
		
		return "redirect:/papers/" + id;
	}
	
	@PostMapping(value = "/delete_paper_reviewer/{id}")
	public String deletePaperReviewer(@PathVariable("id") Long id, @RequestParam Long idReviewer) {
		Optional<Paper> paper = paperService.getPaper(id);
		paper.orElseThrow(PaperNotFoundException::new);
		
		Optional<Reviewer> reviewer = reviewerService.getReviewer(idReviewer);
		reviewer.orElseThrow(ReviewerNotFoundException::new);
		
		paperService.deleteReviewer(paper.get(), reviewer.get());
		
		return "redirect:/papers/" + id;
	}
	
	@GetMapping(value = "/papers/add_paper_quote/{id}")
	public ModelAndView addPaperQuote(@PathVariable("id") Long id){
		ModelAndView modelAndView = new ModelAndView();

		Optional<Paper> paper = paperService.getPaper(id);
		paper.orElseThrow(PaperNotFoundException::new);
		modelAndView.addObject("paper", paper.get());	
		
		/**
		* Create a list of papers without the papers which are already in a 
		* relationship with the paper
		*/
		
		Iterable<Paper> alreadyPapers = paperService.getPaperCitations(paper.get());
		Iterable<Paper> allPapers = paperService.getPapers();
		
		List<Paper> papers = new ArrayList<Paper>();		
		for (Paper iterPaper : allPapers) {
			boolean found = false;
			for (Paper alreadyPaper : alreadyPapers) {
				if (iterPaper.equals(alreadyPaper)) {
					found = true;
				}
			}
			if (!found) papers.add(iterPaper);
		}
		
		// remove the paper itself from the list
		papers.remove(paper.get());
		modelAndView.addObject("papers", papers);
		
		modelAndView.setViewName("add_paper_quote");
		return modelAndView;		
	}
	
	@PostMapping(value = "/update_paper_quote/{id}")
	public String updatePaperQuote(@PathVariable("id") Long id, @RequestParam Long idPaper) {
		Optional<Paper> paper = paperService.getPaper(id);
		paper.orElseThrow(PaperNotFoundException::new);
		
		Optional<Paper> quotedPaper = paperService.getPaper(idPaper);
		quotedPaper.orElseThrow(PaperNotFoundException::new);
		
		try{
			paperService.addQuotedPaper(paper.get(), quotedPaper.get());
		} catch(Exception e) {
			throw new PaperAlreadyPresentException();
		}
		return "redirect:/papers/" + id;
	}
	
	@PostMapping(value = "/delete_paper_quote/{id}")
	public String deletePaperQuote(@PathVariable("id") Long id, @RequestParam Long idPaper) {
		Optional<Paper> paper = paperService.getPaper(id);
		paper.orElseThrow(PaperNotFoundException::new);

		Optional<Paper> quotedPaper = paperService.getPaper(idPaper);
		quotedPaper.orElseThrow(PaperNotFoundException::new);
		
		paperService.deleteQuotedPaper(paper.get(), quotedPaper.get());
		
		return "redirect:/papers/" + id;
	}
	
	@GetMapping(value = "/papers/search/")
	public ModelAndView searchPaper(@RequestParam String keyword) {
		ModelAndView modelAndView = new ModelAndView();
		List<Paper> papers = paperService.searchPaper(keyword);		
		modelAndView.addObject("papers", papers);
		modelAndView.addObject("key", keyword);
		modelAndView.setViewName("papers");
		return modelAndView;
	}
	
}
