package com.unishare.paperapp.controller;

import java.util.Optional;
import java.util.List;
import java.util.ArrayList;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.unishare.paperapp.model.Author;
import com.unishare.paperapp.model.Paper;
import com.unishare.paperapp.model.University;

import com.unishare.paperapp.service.AuthorService;
import com.unishare.paperapp.service.PaperService;
import com.unishare.paperapp.service.UniversityService;

import com.unishare.paperapp.exception.AuthorNotFoundException;
import com.unishare.paperapp.exception.AuthorAlreadyDeletedException;
import com.unishare.paperapp.exception.AuthorAlreadyPresentException;
import com.unishare.paperapp.exception.PaperNotFoundException;
import com.unishare.paperapp.exception.PaperNotFoundException;
import com.unishare.paperapp.exception.UpdateQueryFailedException;

@Controller
public class AuthorController {
	
	@Autowired
	private AuthorService authorService;
	@Autowired
	private UniversityService universityService;
	@Autowired
	private PaperService paperService;
	
	@GetMapping("/authors")
	public ModelAndView authors() {
		ModelAndView modelAndView = new ModelAndView();
		Iterable<Author> authors = authorService.getAuthors();

		modelAndView.addObject("authors", authors);
		modelAndView.setViewName("authors");
		
		return modelAndView;
	}
	
	@GetMapping("/authors/{id}")
	public ModelAndView author(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView();
		
		Optional<Author> author = authorService.getAuthor(id);
		author.orElseThrow(AuthorNotFoundException::new);
		modelAndView.addObject("author", author.get());

		List<Paper> papers = author.get().getPapers();
		modelAndView.addObject("papers", papers);

		University university = author.get().getUniversity();
		modelAndView.addObject("university", university);

		modelAndView.setViewName("author");
		
		return modelAndView;
	}
	
	@GetMapping("/authors/delete/{id}")
	public String deleteAuthor(@PathVariable(name = "id") Long id){
		try{
			authorService.deleteAuthor(id);
		} catch(Exception e) {
			throw new AuthorAlreadyDeletedException();
		}
		return "redirect:/authors";
	}
	
	@GetMapping("/add_author")
	public ModelAndView addAuthor(Model model) {
		ModelAndView modelAndView = new ModelAndView();
		
		Author author = new Author();
		modelAndView.addObject("author", author);
		
		Iterable<University> universities = universityService.getUniversities();
		modelAndView.addObject("universities", universities);
		
		modelAndView.setViewName("author_add");
		return modelAndView;
	}
	
	@PostMapping(value = "/save_author")
	public String saveAuthor(@ModelAttribute Author author) {
		try{
			authorService.addAuthor(author);
		} catch (Exception e){
			throw new UpdateQueryFailedException();
		}
		return "redirect:/authors";
	}
	
	@GetMapping(value = "/authors/edit/{id}")
	public ModelAndView editAuthors(@PathVariable("id") Long id){
		ModelAndView modelAndView = new ModelAndView();
		
		Optional<Author> author = authorService.getAuthor(id);
		author.orElseThrow(AuthorNotFoundException::new);
		modelAndView.addObject("author", author.get());

		Iterable<University> universities = universityService.getUniversities();
		modelAndView.addObject("universities", universities);
		modelAndView.setViewName("author_edit");
		
		return modelAndView;		
	}
	
	@PostMapping(value = "/update_author")
	public String updateAuthor(@ModelAttribute Author author) {
		try{
			authorService.updateAuthor(author);
		} catch (Exception e) {
			throw new UpdateQueryFailedException();
		}
		return "redirect:/authors/" + author.getId();
	}

	@GetMapping(value = "/authors/search/")
	public ModelAndView searchAuthor(@RequestParam String keyword) {
		ModelAndView modelAndView = new ModelAndView();
		List<Author> authors = authorService.searchAuthor(keyword);		
		modelAndView.addObject("authors", authors);
		modelAndView.addObject("key", keyword);
		modelAndView.setViewName("authors");
		return modelAndView;
	}

	@GetMapping(value = "/authors/add_author_paper/{id}")
	public ModelAndView addAuthorPaper(@PathVariable("id") Long id){
		ModelAndView modelAndView = new ModelAndView();

		Optional<Author> author = authorService.getAuthor(id);
		author.orElseThrow(AuthorNotFoundException::new);
		modelAndView.addObject("author", author.get());	
		
		/**
		* Create a list of papers without the papers which are already in a 
		* relationship with the author
		*/
		
		Iterable<Paper> alreadyPapers = authorService.getPapers(author.get());
		Iterable<Paper> allPapers = paperService.getPapers();
		
		List<Paper> papers = new ArrayList<Paper>();		
		for (Paper iterPaper : allPapers) {
			boolean found = false;
			for (Paper alreadyPaper : alreadyPapers) {
				if (iterPaper.equals(alreadyPaper)) {
					found = true;
				}
			}
			if (!found) papers.add(iterPaper);
		}
			
		modelAndView.addObject("papers", papers);
		
		modelAndView.setViewName("add_author_paper");
		return modelAndView;		
	}
	
	@PostMapping(value = "/update_author_paper/{id}")
	public String updateAuthorPaper(@PathVariable("id") Long id, @RequestParam Long idPaper) {
		Optional<Author> author = authorService.getAuthor(id);
		author.orElseThrow(AuthorNotFoundException::new);
		
		Optional<Paper> paper = paperService.getPaper(idPaper);
		paper.orElseThrow(PaperNotFoundException::new);
		
		try{
			paperService.addAuthor(paper.get(), author.get());
		} catch(Exception e) {
			throw new AuthorAlreadyPresentException();
		}
		return "redirect:/authors/" + id;
	}

	@PostMapping(value = "/delete_author_paper/{id}")
	public String deletePaperQuote(@PathVariable("id") Long id, @RequestParam Long idPaper) {
		Optional<Author> author = authorService.getAuthor(id);
		author.orElseThrow(AuthorNotFoundException::new);
		
		Optional<Paper> paper = paperService.getPaper(idPaper);
		paper.orElseThrow(PaperNotFoundException::new);
		
		paperService.deleteAuthor(paper.get(), author.get());
		
		return "redirect:/authors/" + id;
	}
	
}
