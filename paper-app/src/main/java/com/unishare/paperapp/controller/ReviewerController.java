package com.unishare.paperapp.controller;

import java.util.Optional;
import java.util.List;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.unishare.paperapp.model.Reviewer;
import com.unishare.paperapp.model.University;
import com.unishare.paperapp.model.Paper;

import com.unishare.paperapp.service.ReviewerService;
import com.unishare.paperapp.service.UniversityService;
import com.unishare.paperapp.service.PaperService;

import com.unishare.paperapp.exception.ReviewerNotFoundException;
import com.unishare.paperapp.exception.ReviewerAlreadyDeletedException;
import com.unishare.paperapp.exception.ReviewerAlreadyPresentException;
import com.unishare.paperapp.exception.PaperNotFoundException;


@Controller
public class ReviewerController {
	
	@Autowired
	private ReviewerService reviewerService;
	@Autowired
	private UniversityService universityService;
	@Autowired
	private PaperService paperService;
	
	@GetMapping("/reviewers")
	public ModelAndView reviewers() {
		ModelAndView modelAndView = new ModelAndView();
		
		Iterable<Reviewer> reviewers = reviewerService.getReviewers();
		modelAndView.addObject("reviewers", reviewers);
		modelAndView.setViewName("reviewers");
		
		return modelAndView;
	}
	
	@GetMapping("/reviewers/{id}")
	public ModelAndView reviewer(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView();
		
		Optional<Reviewer> reviewer = reviewerService.getReviewer(id);
		reviewer.orElseThrow(ReviewerNotFoundException::new);
		modelAndView.addObject("reviewer", reviewer.get());

		List<Paper> papers = reviewer.get().getPapers();
		modelAndView.addObject("papers", papers);

		University university = reviewer.get().getUniversity();
		modelAndView.addObject("university", university);

		modelAndView.setViewName("reviewer");
		return modelAndView;
	}
	
	@GetMapping("/reviewers/delete/{id}")
	public String deleteReviewer(@PathVariable(name = "id") Long id) {
		try{
			reviewerService.deleteReviewer(id);
		} catch(Exception e) {
			throw new ReviewerAlreadyDeletedException();
		}
		return "redirect:/reviewers";
	}
	
	@GetMapping("/add_reviewer")
	public ModelAndView addAuthor(Model model) {
		
		ModelAndView modelAndView = new ModelAndView();
		
		Reviewer reviewer = new Reviewer();
		modelAndView.addObject("reviewer", reviewer);
		
		Iterable<University> universities = universityService.getUniversities();
		modelAndView.addObject("universities", universities);
		
		modelAndView.setViewName("reviewer_add");
		return modelAndView;
	}
	
	@PostMapping(value = "/save_reviewer")
	public String saveReviewer(@ModelAttribute Reviewer reviewer) {
		reviewerService.addReviewer(reviewer);
		return "redirect:/reviewers";
	}
	
	@GetMapping(value = "/reviewers/edit/{id}")
	public ModelAndView editReviewers(@PathVariable("id") Long id){
		ModelAndView modelAndView = new ModelAndView();

		Optional<Reviewer> reviewer = reviewerService.getReviewer(id);
		reviewer.orElseThrow(ReviewerNotFoundException::new);
		modelAndView.addObject("reviewer", reviewer.get());

		Iterable<University> universities = universityService.getUniversities();
		modelAndView.addObject("universities", universities);

		modelAndView.setViewName("reviewer_edit");
		return modelAndView;	
	}
	
	@PostMapping(value = "/update_reviewer")
	public String updateReviewers(@ModelAttribute Reviewer reviewer) {
		reviewerService.updateReviewer(reviewer);
		return "redirect:/reviewers/" + reviewer.getId();
	}

	@GetMapping(value = "/reviewers/search/")
	public ModelAndView searchReviewer(@RequestParam String keyword) {
		ModelAndView modelAndView = new ModelAndView();
		List<Reviewer> reviewers = reviewerService.searchReviewer(keyword);		
		modelAndView.addObject("reviewers", reviewers);
		modelAndView.addObject("key", keyword);
		modelAndView.setViewName("reviewers");
		return modelAndView;
	}

	@GetMapping(value = "/reviewers/add_reviewer_paper/{id}")
	public ModelAndView addReviewerPaper(@PathVariable("id") Long id){
		ModelAndView modelAndView = new ModelAndView();

		Optional<Reviewer> reviewer= reviewerService.getReviewer(id);
		reviewer.orElseThrow(ReviewerNotFoundException::new);
		modelAndView.addObject("reviewer", reviewer.get());	
		
		/**
		* Create a list of papers without the papers which are already in a 
		* relationship with the reviewer
		*/
		
		Iterable<Paper> alreadyPapers = reviewerService.getPapers(reviewer.get());
		Iterable<Paper> allPapers = paperService.getPapers();
		
		List<Paper> papers = new ArrayList<Paper>();		
		for (Paper iterPaper : allPapers) {
			boolean found = false;
			for (Paper alreadyPaper : alreadyPapers) {
				if (iterPaper.equals(alreadyPaper)) {
					found = true;
				}
			}
			if (!found) papers.add(iterPaper);
		}
			
		modelAndView.addObject("papers", papers);
		
		modelAndView.setViewName("add_reviewer_paper");
		return modelAndView;		
	}
	
	@PostMapping(value = "/update_reviewer_paper/{id}")
	public String updateReviewerPaper(@PathVariable("id") Long id, @RequestParam Long idPaper) {
		Optional<Reviewer> reviewer = reviewerService.getReviewer(id);
		reviewer.orElseThrow(ReviewerNotFoundException::new);
		
		Optional<Paper> paper = paperService.getPaper(idPaper);
		paper.orElseThrow(PaperNotFoundException::new);
		
		try{
			paperService.addReviewer(paper.get(), reviewer.get());
		} catch(Exception e) {
			throw new ReviewerAlreadyPresentException();
		}
		return "redirect:/reviewers/" + id;
	}

	@PostMapping(value = "/delete_reviewer_paper/{id}")
	public String deletePaperQuote(@PathVariable("id") Long id, @RequestParam Long idPaper) {
		Optional<Reviewer> reviewer = reviewerService.getReviewer(id);
		reviewer.orElseThrow(ReviewerNotFoundException::new);
		
		Optional<Paper> paper = paperService.getPaper(idPaper);
		paper.orElseThrow(PaperNotFoundException::new);
		
		paperService.deleteReviewer(paper.get(), reviewer.get());
		
		return "redirect:/reviewers/" + id;
	}
}
