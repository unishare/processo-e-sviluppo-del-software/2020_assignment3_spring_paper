package com.unishare.paperapp.controller;

import java.util.Optional;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.unishare.paperapp.model.University;
import com.unishare.paperapp.model.Person;
import com.unishare.paperapp.model.Paper;

import com.unishare.paperapp.service.UniversityService;
import com.unishare.paperapp.service.PersonService;
import com.unishare.paperapp.service.PaperService;

import com.unishare.paperapp.exception.UniversityNotFoundException;
import com.unishare.paperapp.exception.UniversityAlreadyDeletedException;
import com.unishare.paperapp.exception.PersonNotFoundException;
import com.unishare.paperapp.exception.PersonAlreadyDeletedException;
import com.unishare.paperapp.exception.PersonAlreadyPresentException;


@Controller
public class UniversityController {
	
	@Autowired
	private UniversityService universityService;
	@Autowired
	private PersonService personService;
	@Autowired
	private PaperService paperService;
	
	@GetMapping("/universities")
	public ModelAndView universities() {
		ModelAndView modelAndView = new ModelAndView();
		
		Iterable<University> universities = universityService.getUniversities();
		modelAndView.addObject("universities", universities);
		modelAndView.setViewName("universities");
		
		return modelAndView;
	}
	
	@GetMapping("/universities/{id}")
	public ModelAndView university(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView();
		
		Optional<University> university = universityService.getUniversity(id);
		university.orElseThrow(UniversityNotFoundException::new);
		modelAndView.addObject("university", university.get());
		
		Iterable<Person> people = personService.getPeopleByUniversity(id);
		modelAndView.addObject("people", people);
		
		Set<Paper> papers = paperService.getPapersByUniversity(university.get());
		modelAndView.addObject("papers", papers);
		
		modelAndView.setViewName("university");
		return modelAndView;
	}
	
	@GetMapping("/universities/delete/{id}")
	public String deleteUniversity(@PathVariable("id") Long id) {
		Iterable<Person> peopleOfUniversity = personService.getPeopleByUniversity(id);
		try{
			universityService.deleteUniversity(id, peopleOfUniversity);
		} catch(Exception e) {
			throw new UniversityAlreadyDeletedException();
		}
		return "redirect:/universities";
	}
	
	@GetMapping("/add_university")
	public ModelAndView addUniversity() {
		ModelAndView modelAndView = new ModelAndView();
		
		University university = new University();
		modelAndView.addObject("university", university);

		modelAndView.setViewName("university_add");

		return modelAndView;
	}
	
	@PostMapping(value = "/save_university")
	public String saveUniversity(@ModelAttribute University university) {
		universityService.addUniversity(university);
		return "redirect:/universities";
	}
	
	@GetMapping(value = "/universities/edit/{id}")
	public ModelAndView editUniversity(@PathVariable("id") Long id){
		ModelAndView modelAndView = new ModelAndView();

		Optional<University> university = universityService.getUniversity(id);
		university.orElseThrow(UniversityNotFoundException::new);
		modelAndView.addObject("university", university.get());
		
		modelAndView.setViewName("university_edit");
		return modelAndView;
	}
	
	@PostMapping(value = "/update_university")
	public String updateUniversity(@ModelAttribute University university) {
		universityService.updateUniversity(university);
		return "redirect:/universities/" + university.getId();
	}

	@GetMapping(value = "/universities/search/")
	public ModelAndView searchUniversity(@RequestParam String keyword) {
		ModelAndView modelAndView = new ModelAndView();
		List<University> universities = universityService.searchUniversity(keyword);		
		modelAndView.addObject("universities", universities);
		modelAndView.addObject("key", keyword);
		modelAndView.setViewName("universities");
		return modelAndView;
	}

	@GetMapping(value = "/universities/add_person/{id}")
	public ModelAndView addPerson(@PathVariable("id") Long id){
		ModelAndView modelAndView = new ModelAndView();
		
		Optional<University> university = universityService.getUniversity(id);
		university.orElseThrow(UniversityNotFoundException::new);
		modelAndView.addObject("university", university.get());
		
		Iterable<Person> people = personService.getPeopleWithoutUniversity();				
		modelAndView.addObject("people", people);
		
		modelAndView.setViewName("add_university_person");
		return modelAndView;
	}
	
	@PostMapping(value = "/update_university_person/{id}")
	public String updateUniversityPerson(@PathVariable("id") Long id, @RequestParam Long idPerson) {
		Optional<University> university = universityService.getUniversity(id);
		university.orElseThrow(UniversityNotFoundException::new);
		
		Optional<Person> person = personService.getPerson(idPerson);
		person.orElseThrow(PersonNotFoundException::new);

		try{
			personService.addUniversity(person.get(), university.get());
		} catch(Exception e) {
			throw new PersonAlreadyPresentException();
		}

		return "redirect:/universities/" + id;
	}

	@PostMapping(value = "/delete_university_person/{id}")
	public String deletePaperAuthor(@PathVariable("id") Long id, @RequestParam Long idPerson) {
		Optional<Person> person = personService.getPerson(idPerson);
		person.orElseThrow(PersonNotFoundException::new);
		
		personService.deleteUniversity(person.get());
		
		return "redirect:/universities/" + id;
	} 
}
