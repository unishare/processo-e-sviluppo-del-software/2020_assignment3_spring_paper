package com.unishare.paperapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;

import com.unishare.paperapp.model.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query(value = "SELECT * FROM person WHERE university = ?1", nativeQuery = true)
    public Iterable<Person> findByUniversity(Long id);

    @Query(value = "SELECT * FROM person WHERE university IS NULL", nativeQuery = true)
    public Iterable<Person> findWithoutUniversity();
}