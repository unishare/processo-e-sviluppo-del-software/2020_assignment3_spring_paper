package com.unishare.paperapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;

import com.unishare.paperapp.model.University;

@Repository
public interface UniversityRepository extends JpaRepository<University, Long> {
    @Query(value = "SELECT * FROM university WHERE university.name LIKE %?1% OR university.country LIKE %?1% OR university.city LIKE %?1% ", nativeQuery = true)
    public List<University> search(String keyword);
}
