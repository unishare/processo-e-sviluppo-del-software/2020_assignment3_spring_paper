package com.unishare.paperapp.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;

import com.unishare.paperapp.model.Paper;

@Repository
public interface PaperRepository extends JpaRepository<Paper, Long> {

    @Query(value = "SELECT * FROM paper WHERE paper.title LIKE %?1% OR paper.sub_title LIKE %?1% OR paper.doi LIKE ?2 ", nativeQuery = true)
    public List<Paper> search(String keyword, String doi);
}
