package com.unishare.paperapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import com.unishare.paperapp.model.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
    @Query(value = "SELECT * FROM person WHERE person.type = 'author' AND (person.name LIKE %?1% OR person.surname LIKE %?1%)", nativeQuery = true)
    public List<Author> search(String keyword);
}
