package com.unishare.paperapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;

import com.unishare.paperapp.model.Reviewer;

@Repository
public interface ReviewerRepository extends JpaRepository<Reviewer, Long> {
    @Query(value = "SELECT * FROM person WHERE person.type = 'reviewer' AND (person.name LIKE %?1% OR person.surname LIKE %?1%)", nativeQuery = true)
    public List<Reviewer> search(String keyword);
}
