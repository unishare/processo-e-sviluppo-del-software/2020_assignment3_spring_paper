package com.unishare.paperapp.service;

import java.util.Optional;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unishare.paperapp.model.University;
import com.unishare.paperapp.model.Person;

import com.unishare.paperapp.repository.UniversityRepository;
import com.unishare.paperapp.repository.PersonRepository;
import javax.transaction.Transactional;

@Service
public class UniversityService {
    
    @Autowired
    private UniversityRepository universityRepository;
    @Autowired
    private PersonRepository personRepository;

    public Iterable<University> getUniversities(){          
	    return universityRepository.findAll();
    }

    public Optional<University> getUniversity(Long id){   
	    return universityRepository.findById(id);
    }

    public void deleteUniversity(Long id, Iterable<Person> peopleOfUniversity){

        // Set null on every university of people of this university
        for (Person person : peopleOfUniversity) {
            person.setUniversity(null);
        }
        // and then delete the university
        universityRepository.deleteById(id);
    }

    public void addUniversity(University university){
        universityRepository.save(university);
    }
    
    public void updateUniversity(University university){
	    universityRepository.save(university);
    }

    public List<University> searchUniversity(String keyword){
        return universityRepository.search(keyword);
    }
}
