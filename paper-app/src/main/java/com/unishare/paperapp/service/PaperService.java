package com.unishare.paperapp.service;

import java.util.Optional;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unishare.paperapp.model.*;
import com.unishare.paperapp.repository.PaperRepository;

@Service
public class PaperService {

    @Autowired
    private PaperRepository paperRepository;

    public Iterable<Paper> getPapers(){
        return paperRepository.findAll();
    }

    public Optional<Paper> getPaper(Long id){   
	    return paperRepository.findById(id);
    }

    public void deletePaper(Long id){
        paperRepository.deleteById(id);
    }

    public void addPaper(Paper paper){
        paperRepository.save(paper);
    }

    public void updatePaper(Paper paper){
	    paperRepository.save(paper);
    }

    public Iterable<Paper> getPaperCitations(Paper paper){
	    return paper.getPaperCitations();
    }

    public Iterable<Paper> getPaperQuotations(Paper paper){
	    return paper.getPaperQuotations();
    }


    public Set<Paper> getPapersByUniversity(University university){
        Set<Paper> papersByUniversity = new HashSet<Paper>();   
        List<Person> people = university.getPeople();
        
        /** 
        * For a university cycle every author and add their papers
        * Using a set guarantees that there are no duplicates 
        */
        for (Person person : people) {
            if(person.getType().equals("author")){
                Author author = (Author)person;
                List <Paper> papers = author.getPapers();
                for (Paper paper : papers) {
                    papersByUniversity.add(paper);
                }
            }
        }
        return papersByUniversity;
    }

    public void addAuthor(Paper paper, Author author){
        paper.getAuthors().add(author);
        paperRepository.save(paper);
    }

    public void addReviewer(Paper paper, Reviewer reviewer){
        paper.getReviewers().add(reviewer);
        paperRepository.save(paper);
    }

    public void deleteAuthor(Paper paper, Author author){
        paper.getAuthors().remove(author);
        paperRepository.save(paper);
    }

    public void deleteReviewer(Paper paper, Reviewer reviewer){
        paper.getReviewers().remove(reviewer);
        paperRepository.save(paper);
    }

    public void addQuotedPaper(Paper paper, Paper newPaper){
        paper.getPaperCitations().add(newPaper);
        paperRepository.save(paper);
    }

    public void deleteQuotedPaper(Paper paper, Paper newPaper){
        paper.getPaperCitations().remove(newPaper);
        paperRepository.save(paper);
    }

    /**
     * https://stackoverflow.com/questions/37555675/spring-data-jpa-or-with-single-parameter
     */
    public List<Paper> searchPaper(String keyword){
        return paperRepository.search(keyword, keyword);
    }
}
