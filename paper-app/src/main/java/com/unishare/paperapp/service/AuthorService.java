package com.unishare.paperapp.service;

import java.util.Optional;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Service;

import com.unishare.paperapp.model.Author;
import com.unishare.paperapp.model.Paper;

import com.unishare.paperapp.repository.AuthorRepository;

@Service
public class AuthorService {
    @Autowired
    private AuthorRepository authorRepository;

    public Iterable<Author> getAuthors(){
	    return authorRepository.findAll();
    }
    
    public Optional<Author> getAuthor(Long id){
        return authorRepository.findById(id);
    }

    public void deleteAuthor(Long id){
        authorRepository.deleteById(id);
    }

    public void addAuthor(Author author){
        authorRepository.save(author);
    }

    public void updateAuthor(Author author){
	    authorRepository.save(author);
    }

    public List<Author> searchAuthor(String keyword){
        return authorRepository.search(keyword);
    }

    public List<Paper> getPapers(Author author){
        return author.getPapers();
    }
}
