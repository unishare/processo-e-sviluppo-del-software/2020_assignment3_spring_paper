package com.unishare.paperapp.service;

import java.util.Optional;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unishare.paperapp.model.Reviewer;
import com.unishare.paperapp.model.Paper;

import com.unishare.paperapp.repository.ReviewerRepository;

@Service
public class ReviewerService {
    @Autowired
    private ReviewerRepository reviewerRepository;

    public Iterable<Reviewer> getReviewers(){
	    return reviewerRepository.findAll();
    }

    public Optional<Reviewer> getReviewer(Long id){
        return reviewerRepository.findById(id);
    }
    
    public void deleteReviewer(Long id){
        reviewerRepository.deleteById(id);
    }

    public void addReviewer(Reviewer reviewer){
        reviewerRepository.save(reviewer);
    }

    public void updateReviewer(Reviewer reviewer){
	    reviewerRepository.save(reviewer);
    }

    public List<Reviewer> searchReviewer(String keyword){
        return reviewerRepository.search(keyword);
    }

    public List<Paper> getPapers(Reviewer reviewer){
        return reviewer.getPapers();
    }
}
