package com.unishare.paperapp.service;

import java.util.Optional;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Service;

import com.unishare.paperapp.model.Person;
import com.unishare.paperapp.model.University;
import com.unishare.paperapp.repository.PersonRepository;
import com.unishare.paperapp.repository.UniversityRepository;
import com.unishare.paperapp.service.PersonService;

@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private UniversityRepository universityRepository;

    public Optional<Person> getPerson(Long id){
        return personRepository.findById(id);
    }

    public Iterable<Person> getPeopleByUniversity(Long id){
        return personRepository.findByUniversity(id);
    }

    public Iterable<Person> getPeopleWithoutUniversity(){
        return personRepository.findWithoutUniversity();
    }

    public void addUniversity(Person person, University university){
        
        person.setUniversity(university);
        personRepository.save(person);
        
    }

    public void deleteUniversity(Person person){
        person.setUniversity(null);
        personRepository.save(person);
    }
}
