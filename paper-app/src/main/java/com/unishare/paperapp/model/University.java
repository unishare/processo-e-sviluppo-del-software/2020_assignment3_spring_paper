package com.unishare.paperapp.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;

@Entity
@Table(name = "university")
public class University{
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", unique = true)
  private Long id;

  @Column(name = "name", unique = true, nullable = false)
  private String name;
  @Column(name = "country", nullable = true)
  private String country;
  @Column(name = "city", nullable = true)
  private String city;

  @OneToMany(mappedBy = "university", fetch = FetchType.LAZY)
  private List<Person> people;
    
  public University() {
  }

  public University(String name, String country, String city) {
    this.name = name;
    this.country = country;
    this.city = city;
  }

  @Override
  public String toString() {
    return String.format("University[name=%s, country='%s', city='%s']", name, country, city);
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getCountry() {
    return country;
  }

  public String getCity() {
    return city;
  }

  public List<Person> getPeople(){
    return people;
  }

  public void setId(Long id){
    this.id = id;
  }

  public void setName(String name){
    this.name = name;
  }

  public void setCountry(String country){
    this.country = country;
  }

  public void setCity(String city){
    this.city = city;
  }

  public void setPeople(List<Person> people){
    this.people = people;
  }
  
  public boolean equals(University university){
      if (this.id == university.id)
        return true;
      else
        return false;
  }
  
}
