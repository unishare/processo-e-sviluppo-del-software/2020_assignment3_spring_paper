package com.unishare.paperapp.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "paper")
public class Paper {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "doi", unique = true, nullable = false)
    private String doi;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "sub_title", nullable = true)
    private String subTitle;

    @Column(name = "pub_date", nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate pubDate;

    @Column(name = "ncit", nullable = false)
    private int ncit;

    @ManyToMany
	@JoinTable(
			name="paper_to_paper",
			joinColumns={@JoinColumn(name = "paper")},
            inverseJoinColumns={@JoinColumn(name = "quoted")})
    private List<Paper> paperCitations; 
    
    @ManyToMany(mappedBy="paperCitations")
	private List<Paper> paperQuotations;

    @ManyToMany
	@JoinTable(
			name="paper_to_author",
			joinColumns={@JoinColumn(name = "paper")},
			inverseJoinColumns={@JoinColumn(name = "author")})
    private List<Author> authors;

    @ManyToMany
	@JoinTable(
			name="paper_to_reviewer",
			joinColumns={@JoinColumn(name = "paper")},
			inverseJoinColumns={@JoinColumn(name = "reviewer")})
	private List<Reviewer> reviewers;

    public Paper() {}

    public Paper(String doi, String title, String subTitle, LocalDate pubDate, int ncit) {
        this.doi = doi;
        this.title = title;
        this.subTitle=subTitle;
        this.pubDate = pubDate;
        this.ncit = ncit;
    }

    public Long getId() { 
        return id;
    }

    public String getDoi() { 
        return doi;
    }

    public String getTitle() { 
        return title; 
    }

    public String getSubTitle() { 
        return subTitle; 
    }

    public LocalDate getPubDate() { 
        return pubDate; 
    }

    /** 
     * getter for date casted in String type as "yyyy-mm-dd"
     */ 
    public String getFormattedPubDate(){
        String str;

        if (pubDate != null) {
            str = pubDate.toString();
        }
        else {
            str = "";
        }
	    return str;
    }

    public int getNcit() { 
        return ncit; 
    }

    public List<Paper> getPaperCitations(){
        return paperCitations;
    }
    
    public List<Paper> getPaperQuotations(){
        return paperQuotations;
    }

    public List<Author> getAuthors(){
        return authors;
    }

    public List<Reviewer> getReviewers(){
        return reviewers;
    }

    public void setId(Long id){
        this.id = id;
    }

    public void setDoi(String doi) { 
        this.doi = doi; 
    }

    public void setTitle(String title) { 
        this.title = title; 
    }

    public void setSubTitle(String subTitle) { 
        this.subTitle = subTitle; 
    }

    /**
     * setter for a date with input in String type
     */
    public void setPubDate(String pubDateStr) throws Exception { 
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate pubDate = LocalDate.parse(pubDateStr, dateTimeFormatter);  
        this.pubDate = pubDate; 
    }

    public void setPubDate(LocalDate pubDate) { 
        this.pubDate = pubDate; 
    }

    public void setNcit(int ncit) { 
        this.ncit = ncit; 
    }

    public void setPaperCitations(List <Paper> paperCitations){
        this.paperCitations = paperCitations;
    }

    public void setPaperQuotations(List <Paper> paperQuotations){
        this.paperQuotations = paperQuotations;
    }
    
    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public void setReviewers(List<Reviewer> reviewers) {
        this.reviewers = reviewers;
    }
    

    @Override
    public String toString() {
        return "Paper [doi=" + doi + ", getTitle()=" + getTitle()
          + ", getSubTitle()=" + getSubTitle() + "]";
    }

    public boolean equals(Paper paper){
        if (this.id == paper.id)
          return true;
        else
          return false;
    }
}