package com.unishare.paperapp.model;

import java.time.LocalDate;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.DiscriminatorValue;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
@DiscriminatorValue("reviewer")
public class Reviewer extends Person {
    
    @Column(name = "nrev", nullable = true)
    private int nrev;

    @ManyToMany
	@JoinTable(
			name="paper_to_reviewer",
			joinColumns={@JoinColumn(name = "reviewer")},
			inverseJoinColumns={@JoinColumn(name = "paper")})
	private List<Paper> papers;

    public Reviewer(){}
    public Reviewer(String name, String surname, LocalDate date, int nrev){
        super(name, surname, date);
	    this.nrev = nrev;
    }
    
    public int getNrev(){
	    return nrev;
    }

    public List<Paper> getPapers(){
        return papers;
    }

    public void setNrev(int nrev){
	    this.nrev = nrev;
    }

    public void setPapers(List<Paper> papers) {
        this.papers = papers;
    }
    
    @Override
    public String toString() {
        return "Reviewer [id=" + super.getId() + ", name=" + super.getName()
            + ", surname=" + super.getSurname() + ", birth= " + super.getBirth() 
            + ", nrev=" + nrev + "]";
    }
}
