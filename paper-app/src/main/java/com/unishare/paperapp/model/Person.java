package com.unishare.paperapp.model;

import java.time.LocalDate;
import java.time.format.FormatStyle;
import java.time.format.DateTimeFormatter;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;

@Entity(name = "person")
@Table(name = "person")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type")
public class Person{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;
    
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "surname", nullable = false)
    private String surname;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "birth", nullable = true)
    private LocalDate birth;

    @Column(name = "type", insertable = false, updatable = false)
    private String type;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "university", referencedColumnName = "id", nullable = true) 
    private University university;

    protected Person(){}

    public Person(String name, String surname, LocalDate date){
	    this.name = name;
	    this.surname = surname;
	    this.birth = date;
    }
    
    public Long getId(){
	    return id;
    }
    
    public String getName(){
	    return name;
    }
   
    public String getSurname(){
	    return surname;
    }
    
    public LocalDate getBirth(){
        return birth;
    }

    public String getType(){
        return type;
    }

    /** 
     * getter for date casted in String type as "yyyy-mm-dd"
     */ 
    public String getFormattedBirth(){   

        String str;

        if (birth != null) {
            str = birth.toString();
        }
        else {
            str = "";
        }
	    return str;
    }

    public University getUniversity(){
        return university;
    }

    public void setId(Long id){
        this.id = id;
    } 


    public void setName(String name){
        this.name = name;
    }
   
    public void setSurname(String surname){
	    this.surname = surname;
    }

    /**
     * setter for a date with input in String type
     */
    public void setBirth(String birthStr) throws Exception {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate birth = LocalDate.parse(birthStr, dateTimeFormatter);  
        this.birth = birth; 
    }

    public void setBirth(LocalDate birth) { 
        this.birth = birth; 
    }

    public void setUniversity(University university){
        this.university = university;
    }

    @Override
    public String toString() {
        return "Person [id=" + id + ", name=" + name
	    + ", surname=" + surname + ", birth= " + birth 
        + ", type=" + type + "]";
    }

    public boolean equals(Person person){
        if (this.id == person.id)
          return true;
        else
          return false;
    }
}
