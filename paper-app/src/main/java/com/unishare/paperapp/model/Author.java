package com.unishare.paperapp.model;

import java.time.LocalDate;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToMany;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;

@Entity
@DiscriminatorValue("author")
public class Author extends Person {
    
    @Column(name = "hIndex", nullable = true)
    private Integer hIndex;

    @Column(name = "i10Index", nullable = true)
    private Integer i10Index;

    @Column(name = "ncit", nullable = true)
    private Integer ncit;

    @ManyToMany
	@JoinTable(
			name="paper_to_author",
			joinColumns={@JoinColumn(name = "author")},
			inverseJoinColumns={@JoinColumn(name = "paper")})
    private List<Paper> papers;
    
    public Author(){}
    
    public Author(String name, String surname, LocalDate date, Integer hIndex, Integer i10Idnex, Integer ncit){
        super(name, surname, date);
	    this.hIndex = hIndex;
	    this.i10Index = i10Idnex;
	    this.ncit = ncit;
    }

    public Integer getHIndex(){
    	return hIndex;
    }
    
    public Integer getI10Index(){
	    return i10Index;
    }
    
    public Integer getNcit(){
	    return ncit;
    }
    
    public List<Paper> getPapers(){
        return papers;
    }
    
    public void setHIndex(Integer hIndex){
    	this.hIndex = hIndex;
    }
    
    public void setI10Index(Integer i10Index){
    	this.i10Index = i10Index;
    }  
    
    public void setNcit(Integer ncit){
    	this.ncit = ncit;
    }

    public void setPapers(List<Paper> papers) {
        this.papers = papers;
    }

    @Override
    public String toString() {
        return "Author [id=" + super.getId() + ", name=" + super.getName()
            + ", surname=" + super.getSurname() + ", birth= " + super.getBirth() 
            + ", hIndex=" + hIndex + ", i10Index=" + i10Index 
            + ", ncit= " + ncit + "]";
    }
    
}
