package com.unishare.paperapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason="Paper already present (concurrency conflict)")
public class PaperAlreadyPresentException extends RuntimeException {
    
    private static final long serialVersionUID = -8252953614783756469L;
    
    public PaperAlreadyPresentException() {
        super();
    }
    
    public PaperAlreadyPresentException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public PaperAlreadyPresentException(String message) {
        super(message);
    }
    
    public PaperAlreadyPresentException(Throwable cause) {
        super(cause);
    }
}
