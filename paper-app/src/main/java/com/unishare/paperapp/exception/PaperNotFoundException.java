package com.unishare.paperapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="No such Paper")
public class PaperNotFoundException extends RuntimeException {
    
    private static final long serialVersionUID = -8252953614783756469L;
    
    public PaperNotFoundException() {
        super();
    }
    
    public PaperNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public PaperNotFoundException(String message) {
        super(message);
    }
    
    public PaperNotFoundException(Throwable cause) {
        super(cause);
    }
}
