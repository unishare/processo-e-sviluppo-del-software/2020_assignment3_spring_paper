package com.unishare.paperapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason="Author already deleted (concurrency conflict)")
public class AuthorAlreadyDeletedException extends RuntimeException {
    
    private static final long serialVersionUID = -8252953614783756469L;
    
    public AuthorAlreadyDeletedException() {
        super();
    }
    
    public AuthorAlreadyDeletedException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public AuthorAlreadyDeletedException(String message) {
        super(message);
    }
    
    public AuthorAlreadyDeletedException(Throwable cause) {
        super(cause);
    }
}
