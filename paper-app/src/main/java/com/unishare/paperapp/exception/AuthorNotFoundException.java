package com.unishare.paperapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="No such author")
public class AuthorNotFoundException extends RuntimeException {
    
    private static final long serialVersionUID = -8252953614783756469L;
    
    public AuthorNotFoundException() {
        super();
    }
    
    public AuthorNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public AuthorNotFoundException(String message) {
        super(message);
    }
    
    public AuthorNotFoundException(Throwable cause) {
        super(cause);
    }
}
