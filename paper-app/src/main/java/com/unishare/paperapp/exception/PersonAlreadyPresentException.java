package com.unishare.paperapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason="Person already present (concurrency conflict)")
public class PersonAlreadyPresentException extends RuntimeException {
    
    private static final long serialVersionUID = -8252953614783756469L;
    
    public PersonAlreadyPresentException() {
        super();
    }
    
    public PersonAlreadyPresentException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public PersonAlreadyPresentException(String message) {
        super(message);
    }
    
    public PersonAlreadyPresentException(Throwable cause) {
        super(cause);
    }
}
