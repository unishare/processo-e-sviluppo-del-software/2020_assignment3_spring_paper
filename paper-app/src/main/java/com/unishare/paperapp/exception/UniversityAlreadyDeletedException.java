package com.unishare.paperapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason="University already deleted (concurrency conflict)")
public class UniversityAlreadyDeletedException extends RuntimeException {
    
    private static final long serialVersionUID = -8252953614783756469L;
    
    public UniversityAlreadyDeletedException() {
        super();
    }
    
    public UniversityAlreadyDeletedException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public UniversityAlreadyDeletedException(String message) {
        super(message);
    }
    
    public UniversityAlreadyDeletedException(Throwable cause) {
        super(cause);
    }
}
