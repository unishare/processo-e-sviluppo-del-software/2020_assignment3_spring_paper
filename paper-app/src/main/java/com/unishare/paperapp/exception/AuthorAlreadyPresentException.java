package com.unishare.paperapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason="Author already present (concurrency conflict)")
public class AuthorAlreadyPresentException extends RuntimeException {
    
    private static final long serialVersionUID = -8252953614783756469L;
    
    public AuthorAlreadyPresentException() {
        super();
    }
    
    public AuthorAlreadyPresentException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public AuthorAlreadyPresentException(String message) {
        super(message);
    }
    
    public AuthorAlreadyPresentException(Throwable cause) {
        super(cause);
    }
}
