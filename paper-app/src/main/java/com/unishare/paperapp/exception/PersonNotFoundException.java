package com.unishare.paperapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="No such person")
public class PersonNotFoundException extends RuntimeException {
    
    private static final long serialVersionUID = -8252953614783756469L;
    
    public PersonNotFoundException() {
        super();
    }
    
    public PersonNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public PersonNotFoundException(String message) {
        super(message);
    }
    
    public PersonNotFoundException(Throwable cause) {
        super(cause);
    }
}
