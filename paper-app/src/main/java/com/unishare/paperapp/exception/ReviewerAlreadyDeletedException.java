package com.unishare.paperapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason="Reviewer already deleted (concurrency conflict)")
public class ReviewerAlreadyDeletedException extends RuntimeException {
    
    private static final long serialVersionUID = -8252953614783756469L;
    
    public ReviewerAlreadyDeletedException() {
        super();
    }
    
    public ReviewerAlreadyDeletedException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public ReviewerAlreadyDeletedException(String message) {
        super(message);
    }
    
    public ReviewerAlreadyDeletedException(Throwable cause) {
        super(cause);
    }
}
