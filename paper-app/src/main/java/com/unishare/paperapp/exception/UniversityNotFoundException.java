package com.unishare.paperapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="No such university")
public class UniversityNotFoundException extends RuntimeException {
    
    private static final long serialVersionUID = -8252953614783756469L;
    
    public UniversityNotFoundException() {
        super();
    }
    
    public UniversityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public UniversityNotFoundException(String message) {
        super(message);
    }
    
    public UniversityNotFoundException(Throwable cause) {
        super(cause);
    }
}
