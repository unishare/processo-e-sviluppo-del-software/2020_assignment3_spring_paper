package com.unishare.paperapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason="University already present")
public class UniversityAlreadyPresentException extends RuntimeException {
    
    private static final long serialVersionUID = -8252953614783756469L;
    
    public UniversityAlreadyPresentException() {
        super();
    }
    
    public UniversityAlreadyPresentException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public UniversityAlreadyPresentException(String message) {
        super(message);
    }
    
    public UniversityAlreadyPresentException(Throwable cause) {
        super(cause);
    }
}
