package com.unishare.paperapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="No such reviewer")
public class ReviewerNotFoundException extends RuntimeException {
    
    private static final long serialVersionUID = -8252953614783756469L;
    
    public ReviewerNotFoundException() {
        super();
    }
    
    public ReviewerNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public ReviewerNotFoundException(String message) {
        super(message);
    }
    
    public ReviewerNotFoundException(Throwable cause) {
        super(cause);
    }
}
