package com.unishare.paperapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason="Person already deleted (concurrency conflict)")
public class PersonAlreadyDeletedException extends RuntimeException {
    
    private static final long serialVersionUID = -8252953614783756469L;
    
    public PersonAlreadyDeletedException() {
        super();
    }
    
    public PersonAlreadyDeletedException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public PersonAlreadyDeletedException(String message) {
        super(message);
    }
    
    public PersonAlreadyDeletedException(Throwable cause) {
        super(cause);
    }
}
