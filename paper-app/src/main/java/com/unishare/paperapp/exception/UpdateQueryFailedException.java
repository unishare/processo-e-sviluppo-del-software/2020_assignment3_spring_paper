package com.unishare.paperapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN, reason="The update query is failed.")
public class UpdateQueryFailedException extends RuntimeException {
    
    private static final long serialVersionUID = -8252953614783756469L;
    
    public UpdateQueryFailedException() {
        super();
    }
    
    public UpdateQueryFailedException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public UpdateQueryFailedException(String message) {
        super(message);
    }
    
    public UpdateQueryFailedException(Throwable cause) {
        super(cause);
    }
}
