package com.unishare.paperapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaperappApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaperappApplication.class, args);
	}

}
