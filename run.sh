# Create network
echo "Create network assignment3.network..."
docker network create assignment3.network
echo "Network assignment3.network created."

# Create db
cd paper-db
echo "Starting db..."
docker-compose up --build -d
cd ..

# Run app
cd paper-app
echo "Starting app..."
./mvnw clean package spring-boot:repackage
java -Djava.security.egd=file:/dev/./urandom -jar target/paperapp-0.0.1-SNAPSHOT.jar
echo "App closed."
cd ..

# Close db
cd paper-db
echo "Stopping db..."
docker-compose down
cd ..

# Remove network
echo "Remove network assignment3.network..."
docker network rm assignment3.network
echo "Network assignment3.network removed."
