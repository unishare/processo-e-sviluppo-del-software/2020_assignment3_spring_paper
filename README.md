# Assignment 3

## Informazioni generiche

### Repository link

- [https://gitlab.com/unishare/processo-e-sviluppo-del-software/2020_assignment3_spring_paper](https://gitlab.com/unishare/processo-e-sviluppo-del-software/2020_assignment3_spring_paper)

### Autori

- 829470 Federica Di Lauro
- 829827 Davide Cozzi 
- 829835 Gabriele De Rosa

## Avvio completo

### Versione ridotta
Per l'esecuzione dell'applicazione è stato inserito un file `.sh` 
per l'avvio semplificato. Tale script si occupa di avviare in sequenza 
il database e l'applicazione; nel dettaglio procede ad eseguire tutti i comandi 
elencati nella [versione estesa](#versione-estesa) dell'avvio.

```
./run.sh
```
Al primo avvio è indispensabile procedere con 
l'[inizializzazione del database](#inizializzazione-del-db).

### Versione estesa
Qualora si scegliesse la versione di avvio estesa bisognerebbe seguire i 
seguenti comandi.

#### Avvio del database
```
cd paper-db
```
È necessario creare una sottorete docker per le immagini che saranno avviate 
in seguito.
```
docker network create assignment3.network
```
In seguito è possibile avviare i docker container necessari mediante un 
[docker-compose](https://docs.docker.com/compose/).
```
docker-compose up --build -d
```
#### Avvio dell'applicazione
```
cd paper-app
```
```
./mvnw clean package spring-boot:repackage
```
```
java -Djava.security.egd=file:/dev/./urandom -jar target/paperapp-0.0.1-SNAPSHOT.jar
```
Al primo avvio è indispensabile procedere 
all'[inizializzazione del database](#inizializzazione-del-db).


### Inizializzazione del db
Alla prima esecuzione è necessario inizializzare il database mediante un file 
`.sql`. Per la semplificazione di tale compito è stato adibito un 
pannello _PhpMyAdmin_ all'indirizzo [localhost:8081](localhost:8081) 
al quale è possibile accedere mediante le seguenti credenziali 
(presenti nel `docker-compose.yml`):
```
username = unishare
password = abc123456789
```
Importare il file `paper-db/db_init.sql` all'interno del database `my_db`.

# Descrizione dettagliata del progetto

## Descrizione dell'applicazione
L'applicazione si propone di fornire un pannello di amministrazione per un 
portale di gestione paper.  Nel portale sono fornite le funzionalità CRUD per 
i paper, gli autori, i reviewer e le università.

Consideriamo alcune scelte per l'applicazione:
- Ogni paper può avere più autori e più reviewer, e può citare più paper.
- Ogni autore e ogni reviewer fa riferimento ad una sola università.
- Ad ogni università appartengono più autori e più reviewer.

![Schema ER del database](./paper-db/er/er.png)

Nella realizzazione effettiva del database si è scelto di gestire l'ereditarietà 
tramite una single-table "_person_" che contenga entrambe le tabelle 
figlie "_author_" e "_reviewer_".

> Siamo coscienti del fatto che la nostra implementazione non sia completamente 
realistica e fedele a quanto avvenga realmente. 
In situazioni reali infatti una persona può essere contemporaneamente 
autore per alcuni paper e revisore per altri.
Ai fini di questo assignment invece si è scelto di considerare scientemente 
queste due entità come due entità completamente disgiunte.
Inoltre si è anche a conoscenza del fatto che in situazioni reali i due 
attributi `ncit` (numero di citazioni) e `nrev` (numero di revisioni) sono 
logicamente calcolati dinamicamente a partire dagli altri dati.
Nel nostro caso, con la volontà di aggiungere anche degli attributi numerici, 
sono stati aggiunti staticamente.

A partire dalle classi presentate sono state ottenute con corrispondenza 
diretta le seguenti classi, aggiunte al package `model`:
- `Paper`
- `Person`
- `Author`
- `Reviewer`
- `University`

In ognuna di queste classi sono stati definiti gli attributi dell'entità e le 
annotazioni che permettono di gestire la persistenza dei dati sul db tramite 
Spring Data.
Come anticipato, le entità Author e Reviewer sono gestite tramite strategia 
"single-table" sulla tabella `Person`. La differenziazione delle entità è 
gestita utilizzando il valore contenuto nell'attributo `type` 
come discriminator-value.

Per la gestione della persistenza sono state aggiunte, nel package `repository`, 
le seguenti classi:
- `PaperRepository`
- `PersonRepository`
- `AuthorRepository`
- `ReviewerRepository`
- `UniversityRepository`

Tutte le classi appena elencate estendono `JpaRepository` per sfruttare i 
metodi standard CRUD di Spring Data. 
Soltanto alcune query specifiche sono state scritte tramite l'annotazione 
`@Query`.

Per gestire la business logic sono presenti nel package `service` le classi:
- `PaperService`
- `PersonService`
- `AuthorService`
- `ReviewerService`
- `UniversityService`

Queste classi permettono di avere un ulteriore livello di controllo tra 
applicazione e db.

Per gestire le richieste sono presenti nel package `controller` le classi:
- `PaperController`
- `AuthorController`
- `ReviewerController`
- `UniversityController`

Ognuna delle classi si occupa di gestire le richieste relative alle entità di 
riferimento. 
Viene sfruttato lo strato `Service` per la gestione dei dati.
Nel caso si abbiano degli scenari abnormali vengono lanciate delle eccezioni e
si viene reindirizzati alla pagina d'errore.
La gestione delle `view` invece avviene sfruttando la classe `ModelAndView` che 
permette il passaggio degli oggetti direttamente al frontend.
La parte di `view` del front-end viene quindi gestita tramite il framework 
`Thymeleaf` e i relativi template.

Per gestire le eccezioni sono presenti nel package `exception` le relative 
classi. Ad esempio alcune classi di tipo "NotFound" nell'eventualità in cui si 
abbia un tentativo di accesso ad un record non presente nel db.
